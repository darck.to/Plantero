(function() {
    $('.navbar-burger').toggleClass('is-active');
    $('.navbar-menu').toggleClass('is-active');
    // SI existe session mandamos el menú de usuario
    compruebaLogin(localStorage.getItem("rep_token"));
}());

function compruebaLogin(t){
    var content = "";
    if(t == 0 || t == null){
        content = '<div class="navbar-item">';
            content += '<div class="buttons">';
                content += '<a class="button is-primary buttonSignup">';
                    content += '<strong>Crea tu Cuenta</strong>';
                content += '</a>';
                content += '<a class="button is-light buttonLogin">';
                    content += 'Inicia Sesi&oacute;n';
                content += '</a>';
            content += '</div>';
        content += '</div>';  
    } else {
        content = "<div class='navbar-item'>";
            content += '<div class="buttons">';
                content += '<a class="button is-medium is-info" href="dashboard.html">';
                    content += '<span class="icon"><i class="fa-solid fa-gauge"></i></span>';
                    content += '<span>Dashboard</span>';
                content += '</a>';
                content += '<a class="button is-danger buttonLogout">';
                    content += '<strong>Cierra Sesi&oacute;n</strong>';
                content += '</a>';
            content += '</div>';
        content += "</div>";  
    }
    $('.navbar-end').html(content);

    // Carga Login
    $('.buttonLogin').on('click', function(e){
        modap();
        $('.modal-content').load('templates/init/init-login.html');
    })
    // Carga Sign up
    $('.buttonSignup').on('click', function(e){
        modap();
        $('.modal-content').load('templates/init/init-signin.html');
    })
    // Login out
    $('.buttonLogout').on('click', function(e) {
        localStorage.removeItem("rep_key");
        localStorage.removeItem("rep_user");
        localStorage.removeItem("rep_token");
        modal('<h1 class="has-text-centered">... cerrando sesi&oacute;n</h1>')
        setTimeout(location.reload.bind(location), 1000);
    })
}
  
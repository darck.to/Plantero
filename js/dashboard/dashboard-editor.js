editor = grapesjs.init({
    container: '.editor',
    fromElement: true,
    width: "auto",
    pageManager: true,
    storageManager:  {
      type: 'indexeddb',
    },
    plugins: ['gjs-preset-webpage','grapesjs-project-manager'],
    pluginsOpts: {
      'gjs-preset-webpage': {
        // options
      },
      'grapesjs-project-manager': {
        // options
      }
    }
});

// Running commands from panels
pn = editor.Panels;
pn.addButton('options', {
    id: 'open-templates',
    className: 'fa fa-folder-o',
    attributes: {
        title: 'Abrir Proyectos y Plantillas'
    },
    command: 'open-templates', //Open modal 
});
pn.addButton('views', {
    id: 'open-pages',
    className: 'fa fa-file-o',
    attributes: {
        title: 'Tomar Captura'
    },
    command: 'open-pages',
    togglable: false
});

editor.Panels.addButton('options', [{
    id: 'save-db',
    className: 'fa fa-floppy-o',
    command: 'save-db',
    attributes: {title: 'Guardar'}
  }]
);
// Add the command
editor.Commands.add('save-db', {
    run: function(editor, sender)
    {
      sender && sender.set('active', 0); // turn off the button
      editor.store();

      var htmldata = editor.getHtml();
      var cssdata = editor.getCss();
      // Liga al usuario
      guardaPlantilla(htmldata,cssdata)
    }
});

function guardaPlantilla(html,css) {
  
  var key = localStorage.getItem("rep_key");
  var usr = localStorage.getItem("rep_user");

  var formData = new FormData();
  formData.append('key', key);
  formData.append('usr', usr);
  formData.append('html', html);
  formData.append('css', css);
  var formMethod = 'Post';
  var rutaScrtip = 'php/dashboard/dashboard-template-guarda.php';

  var request = $.ajax({
    url: rutaScrtip,
    method: formMethod,
    data: formData,
    contentType: false,
    processData: false,
    async: true,
    dataType: 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
  });
  // handle the responses
  request.done(function(data) {
    $.each(data, function (name, value) {
      toast(value.message)
    })
  })
  request.fail(function(jqXHR, textStatus) {
    console.log(textStatus);
  })
  request.always(function(data) {
    // clear the form
    $('#initLoginUser').trigger('reset')
  });
}